
      let fillCircle = document.querySelector(".client__sliderimage")
      let fillCircleStart = 0;
      let fillCircleEnd = 100;
      let speed = 30;
      let newfun = setInterval(() => {
        fillCircleStart++;
        fillCircle.style.background = `conic-gradient(  #212020  ${
          fillCircleStart * 3.6
        }deg, white 0deg)`;
        if (fillCircleStart === fillCircleEnd) {
          fillCircleStart=0
        }
      }, speed);
